﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection dataConnection = new SqlConnection();
            string connString = "Data Source=(localdb)\\ProjectsV13;Initial Catalog=StudentDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            dataConnection.ConnectionString = connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand();
            command.Connection = dataConnection;
            command.CommandText = "SELECT * FROM Student";
            List<Student> listOfStudents = new List<Student>();
            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read()){
                Student student = new Student(dataReader.GetInt32(0),dataReader.GetString(1), dataReader.GetString(2));
                listOfStudents.Add(student);
            }

            dataConnection.Close();

            foreach(Student student in listOfStudents){
                listBox1.Items.Add(student.Id + " " + student.Name + " " + student.Surname);


            }


        }
    }
}
